#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

struct Entry
{
    QString PrevPath;
    QString NewPath;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QVector<Entry> log;

    QVector<QString> GetFolders(QString& path);
    QVector<QString> GetFiles(QString& path);
    void AddDate(QString& path);
    void SaveLog();
};
#endif // MAINWINDOW_H
