#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QDir>
#include <QTextStream>
#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Выбор корневого каталога для добавления даты
    connect(ui->pbRootFolder, &QPushButton::clicked, this, [this]
    {
        QString path = QFileDialog::getExistingDirectory();
        ui->leRootFolder->setText(path);
    });

    //Запуск добавления даты
    connect(ui->pbRun, &QPushButton::clicked, this, [this]
    {
        QString path = ui->leRootFolder->text();
        if (path.isEmpty())
        {
            QMessageBox::information(this, "Warning", "Не задан корневой каталог для замены добавления даты!");
            return;
        }

        AddDate(path);
        SaveLog();

        QMessageBox::information(this, "Warning", "Переименование завершено.");
    });

    //отмена изменений
    connect(ui->pbUndo, &QPushButton::clicked, this, [this]
    {
        QString path = QFileDialog::getOpenFileName();
        QFile logFile(path);
        QVector<Entry> tempLog;
        if (logFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            Entry entry;
            while(!logFile.atEnd())
            {
                QString str = logFile.readLine();
                QStringList lst = str.split("     ");
                if (lst.length() == 2)
                {
                    entry.PrevPath = lst[0];
                    entry.NewPath = lst[1];
                    entry.NewPath.chop(1);
                    tempLog.push_back(entry);
                }
            }
            int length = tempLog.length();
            for(int i = length - 1; i >= 0; i--)
            {
                QFile::rename(tempLog[i].NewPath, tempLog[i].PrevPath);
            }
            QMessageBox::information(this, "Warning", "Изменения отменены.");
        }
        else
        {
            QMessageBox::information(this, "Warning", "Невозможно открыть Log-файл!");
        }
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

QVector<QString> MainWindow::GetFolders(QString &path)
{
    QDir dir(path);
    QVector<QString> dirList;
    dir.setFilter(QDir::Dirs);
    QStringList list = dir.entryList();
    for (auto& entry: list)
    {
        if (entry == "." || entry == "..") continue;
        dirList.push_back(path + "/" + entry);
    }
    return dirList;
}

QVector<QString> MainWindow::GetFiles(QString &path)
{
    QDir dir(path);
    QVector<QString> fileList;
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Time);
    QStringList list = dir.entryList();
    for (auto& entry: list)
    {
        if (entry == "." || entry == "..") continue;
        fileList.push_back(path + "/" + entry);
    }
    return fileList;
}

void MainWindow::AddDate(QString &path)
{
    QVector<QString> fileList;
    fileList = GetFiles(path);
    if (!fileList.isEmpty())
    {
        QString lastFilePath = fileList.last();
        QFileInfo fileInfo(lastFilePath);
        QDateTime fileDateTime = fileInfo.lastModified();
        QDate fileDate = fileDateTime.date();
        QString year = QString::number(fileDate.year());
        QString month = QString::number(fileDate.month());
        if (month.length() < 2)
        {
            month.insert(0, '0');
        }
        QString day = QString::number(fileDate.day());
        if (day.length() < 2)
        {
            day.insert(0, '0');
        }
        QString prevPath = path;
        QDir currentDir(path);
        const QString prevName = currentDir.dirName();
        const QString newName = year + ui->leDivider1->text() + month + ui->leDivider2->text() + day + ui->leDivider3->text() + prevName;
        QString newPath = prevPath.chopped(prevName.length()) + newName;
        if (QFile::rename(prevPath, newPath))
        {
            Entry logRecord;
            logRecord.PrevPath = prevPath;
            logRecord.NewPath = newPath;
            log.push_back(logRecord);
            path = newPath;
        }
        else
        {
            QMessageBox::information(this, "Warning", "Переименование не выполнено!");
        }

    }
    QVector<QString> dirList;
    dirList = GetFolders(path);
    for (auto checkPath: dirList)
    {
        AddDate(checkPath);
    }
}

void MainWindow::SaveLog()
{
    QDate date = QDate::currentDate();
    QTime time = QTime::currentTime();
    QString timeString = time.toString();
    timeString.replace(':', '_');

    QString logFileName = "Log_" + date.toString() + "_" + timeString + ".txt";
    QFile logFile(logFileName);
    if (logFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream writeStream(&logFile);
        for (auto& record: log)
        {
            writeStream << record.PrevPath << "     " << record.NewPath << '\n' << Qt::flush;
        }
        logFile.close();
    }
    else
    {
        QMessageBox::information(this, "Warning", "Лог файл не создан.");
    }
}


